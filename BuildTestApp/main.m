//
//  main.m
//  BuildTestApp
//
//  Created by SiGMan on 29.04.14.
//  Copyright (c) 2014 SiGMan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SGAppDelegate class]));
    }
}
