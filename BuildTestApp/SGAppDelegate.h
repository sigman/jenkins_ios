//
//  SGAppDelegate.h
//  BuildTestApp
//
//  Created by SiGMan on 29.04.14.
//  Copyright (c) 2014 SiGMan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
